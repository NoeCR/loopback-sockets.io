import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../app/services/websocket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'front';

  constructor(
    private wsService: WebsocketService
  ) {
    this.escucharSockets();
  }
// tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {
    this.wsService.emit('message', {message: 'Conectado desde el front'});
  }
  private escucharSockets() {
    this.wsService.listen('message').subscribe(
      response =>{
        console.log(response);
      },
      err => {
        console.log(err);
      }
    );
  }
}
