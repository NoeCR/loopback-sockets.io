const socket = require('socket.io');

//const usersConect = new 
var WebSocket = {
    messages: (socket, io) => {
        socket.on('message', (msg) => {
            console.log(msg);
            io.emit('message', 'Mensaje recibido en el servidor');
        })
    }
}

module.exports = WebSocket;