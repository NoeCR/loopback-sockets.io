'use strict';
// = module.exports
var loopback = require('loopback');
var boot = require('loopback-boot');
var app = loopback();
var ws = require('./socket');
// var http = require('http');
// const WebSocket = require('ws');
// var httpServer = new http.Server(app);
// var wss = new WebSocket.Server({server: httpServer});




app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;


  if (require.main === module) {
    //Comment this app.start line and add following lines
    //app.start();
    app.io = require('socket.io')(app.start());
    // require('socketio-auth')(app.io, {
    //   authenticate: function (socket, value, callback) {
  
    //       var AccessToken = app.models.AccessToken;
    //       //get credentials sent by the client
    //       var token = AccessToken.find({
    //         where:{
    //           and: [{ userId: value.userId }, { id: value.id }]
    //         }
    //       }, function(err, tokenDetail){
    //         if (err) throw err;
    //         if(tokenDetail.length){
    //           callback(null, true);
    //         } else {
    //           callback(null, false);
    //         }
    //       }); //find function..    
    //     } //authenticate function..
    // });
  
    app.io.on('connection', function(socket){
      console.log('a user connected', socket.id);
        // Carga de sockets 
        // Mostrar mensajes
        ws.messages(socket, app.io);


      // socket.on('message', (msg) => {
      //     console.log(msg);
      // })
      socket.on('disconnect', function(){
          console.log('user disconnected');
      });
    });
  // start the server if `$ node server.js`
//   if (require.main === module)
//     app.start();
//     wss.on('connection', function connection(ws) {
//       console.log('User connected');
//       ws.on('message', function incoming(message) {
//         console.log('received: %s', message);
//       });
    
     
//       ws.send('something');
//     });
 }});
